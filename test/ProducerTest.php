<?php


namespace Maxipost\AMQPTest;


use Maxipost\AMQP\Config;
use Maxipost\AMQP\Event;
use Maxipost\AMQP\EventHydrator;
use Maxipost\AMQP\Exchange;
use Maxipost\AMQP\Factory\AMQPExchangeFactory;
use Maxipost\AMQP\Producer;
use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\DomainEventSourcing\DomainEventInterface;
use PHPUnit\Framework\TestCase;

class ProducerTest extends TestCase
{

    private const TEST_MSG = ['testMsg'];
    const TEST_ID = 'testId';
    const TEST_CLASS_NAME = 'testClassName';

    public function testPublish(): void
    {

        $domainEvent = $this->prophesize(DomainEventInterface::class);
        $exchange = $this->prophesize(Exchange::class);
        $eventHydrator = $this->prophesize(EventHydrator::class);
        $AMQPExchangeFactory = $this->prophesize(AMQPExchangeFactory::class);

        $config = $this->prophesize(Config::class);
        $config->getEventIdByClassName($domainEvent->reveal())
            ->willReturn(self::TEST_CLASS_NAME);

        $aggregateRootId = $this->prophesize(AggregateRootId::class);
        $aggregateRootId->__toString()->willReturn(self::TEST_ID);
        $domainEvent->getId()->willReturn($aggregateRootId->reveal());

        $eventHydrator->extract($domainEvent->reveal())
            ->shouldBeCalled()
            ->willReturn([]);

        $AMQPExchange = $this->prophesize(\AMQPExchange::class);
        $AMQPExchange->declareExchange()->shouldBeCalled();
        $event = new Event(self::TEST_CLASS_NAME, self::TEST_ID, self::TEST_MSG);
        $AMQPExchange->publish(json_encode($event))->shouldBeCalled();

        $AMQPExchangeFactory->create($exchange->reveal())
            ->shouldBeCalledOnce()
            ->willReturn($AMQPExchange->reveal());


        (new Producer(
            $eventHydrator->reveal(),
            $AMQPExchangeFactory->reveal(),
            $config->reveal()
        ))->publish($exchange->reveal(), $domainEvent->reveal());
    }
}