<?php
declare(strict_types=1);

namespace Maxipost\AMQP;


use AMQPChannel;
use AMQPQueue;
use function json_decode;

class Queue
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var bool
     */
    private $isAutoAck;
    /**
     * @var AMQPQueue
     */
    private $AMQPQueue;
    /**
     * @var string
     */
    private $tag;

    /**
     * Queue constructor.
     * @param AMQPChannel $AMQPChannel
     * @param Exchange $exchange
     * @param string $clientName
     * @param bool $isAutoAck
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     * @throws \AMQPChannelException
     */
    public function __construct(
        AMQPChannel $AMQPChannel,
        Exchange $exchange,
        string $clientName,
        bool $isAutoAck
    ) {
        $this->isAutoAck = $isAutoAck;
        $this->name = sprintf('%s.%s', $clientName, $exchange->getId());

        $queue = new AMQPQueue($AMQPChannel);
        $queue->setName($this->name);
        $queue->setFlags(AMQP_DURABLE);
        $queue->bind($exchange->getId());
        $queue->declareQueue();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isAutoAck(): bool
    {
        return $this->isAutoAck;
    }

    /**
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     */
    public function ack(): void
    {
        if ($this->tag === null) {
            return;
        }
        $this->AMQPQueue->ack($this->tag);
    }

    public function getEvent(): \Event
    {
        $eventData = json_decode($this->get()->getBody(), true);
        return new Event($eventData['id'] ?? '', $eventData['eventId'] ?? '', $eventData['payload'] ?? []);
    }

    private function get()
    {
        $result = $this->AMQPQueue->get();
        $this->tag = $result->getDeliveryTag();
        return $result;
    }
}