<?php
declare(strict_types=1);

namespace Maxipost\AMQP\Factory;


use AMQPConnection;
use Maxipost\AMQP\Config;

class AMQPConnectionFactory
{

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function create(): AMQPConnection
    {
        return new AMQPConnection([
            'host' => $this->config->getHost(),
            'port' => $this->config->getPort(),
            'vhost' => '/',
            'login' => $this->config->getUser(),
            'password' => $this->config->getPass()
        ]);
    }
}