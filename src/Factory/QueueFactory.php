<?php
declare(strict_types=1);

namespace Maxipost\AMQP\Factory;


use AMQPChannel;
use AMQPQueue;
use function json_decode;
use Maxipost\AMQP\Exchange;
use Maxipost\AMQP\Queue;

class QueueFactory
{

    /**
     * @var AMQPChannel
     */
    private $AMQPChannel;
    /**
     * @var string
     */
    private $clientName;

    public function __construct(AMQPChannel $AMQPChannel, string $clientName)
    {
        $this->AMQPChannel = $AMQPChannel;
        $this->clientName = $clientName;
    }

    public function create(Exchange $exchange, bool $autoAck = true): Queue
    {
        return new Queue($this->AMQPChannel, $exchange, $this->clientName, $autoAck);
    }
}