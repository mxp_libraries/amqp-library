<?php
declare(strict_types=1);

namespace Maxipost\AMQP\Factory;


use AMQPChannel;

class AMQPChannelFactory
{

    /**
     * @var \AMQPConnection
     */
    private $AMQPConnection;

    public function __construct(\AMQPConnection $AMQPConnection)
    {
        $this->AMQPConnection = $AMQPConnection;
    }

    /**
     * @return AMQPChannel
     * @throws \AMQPConnectionException
     */
    public function create(): AMQPChannel
    {
        return new AMQPChannel($this->AMQPConnection);
    }
}