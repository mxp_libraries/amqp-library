<?php
declare(strict_types=1);

namespace Maxipost\AMQP\Factory;


use AMQPChannel;
use AMQPExchange;
use Maxipost\AMQP\Exchange;

class AMQPExchangeFactory
{

    /**
     * @var \AMQPConnection
     */
    private $AMQPConnection;

    public function __construct(\AMQPConnection $AMQPConnection)
    {
        $this->AMQPConnection = $AMQPConnection;
    }

    public function create(Exchange $exchange): AMQPExchange
    {
        $amqpExchange = new AMQPExchange($this->AMQPConnection);
        $amqpExchange->setName($exchange->getId());
        $amqpExchange->setType($exchange->getType());

        return $amqpExchange;
    }
}