<?php


namespace Maxipost\AMQP;


use Maxipost\AMQP\Factory\AMQPExchangeFactory;
use Maxipost\DomainEventSourcing\DomainEventInterface;

class Producer
{
    private $eventHydrator;

    private $AMQPExchangeFactory;

    private $config;

    public function __construct(
        EventHydrator $eventHydrator,
        AMQPExchangeFactory $AMQPExchangeFactory,
        Config $config
    ) {
        $this->eventHydrator = $eventHydrator;
        $this->AMQPExchangeFactory = $AMQPExchangeFactory;
        $this->config = $config;
    }

    public function publish(
        Exchange $exchange,
        DomainEventInterface $domainEvent
    ): void {
        $AMQPExchange = $this->AMQPExchangeFactory->create($exchange);
        $AMQPExchange->declareExchange();

        $event = new Event(
            $this->config->getEventIdByClassName($domainEvent),
            (string)$domainEvent->getId(),
            $this->eventHydrator->extract($domainEvent)
        );

        $AMQPExchange->publish(json_encode($event));
    }


}