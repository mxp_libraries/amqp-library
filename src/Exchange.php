<?php
declare(strict_types=1);

namespace Maxipost\AMQP;


class Exchange
{
    public const ORDERS = 'orders';

    public const EXCHANGES = [
        self::ORDERS => [
            'type' => AMQP_EX_TYPE_FANOUT
        ]
    ];
    /**
     * @var string
     */
    private $id;

    /**
     * Exchange constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return self::EXCHANGES[$this->id]['type'];
    }
}