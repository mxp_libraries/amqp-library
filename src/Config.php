<?php
declare(strict_types=1);

namespace Maxipost\AMQP;


use Exception;
use Maxipost\CoreDomain\EventMap;
use Maxipost\DomainEventSourcing\DomainEventInterface;

class Config
{

    /**
     * @var string
     */
    private $host;
    /**
     * @var int
     */
    private $port;
    /**
     * @var string
     */
    private $user;
    /**
     * @var string
     */
    private $pass;
    /**
     * @var array
     */
    private $eventClassByEventId;

    public function __construct(
        string $host,
        int $port,
        string $user,
        string $pass,
        array $eventClassByEventId = null
    ) {
        if ($eventClassByEventId === null) {
            $eventClassByEventId = EventMap::getDefaultMap();
        }
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->eventClassByEventId = $eventClassByEventId;
    }

    public function getClassNameByEventId(string $eventId): string
    {
        $className = $this->eventClassByEventId[$eventId] ?? null;
        if ($className === null) {
            /** @noinspection ThrowRawExceptionInspection */
            throw new Exception("Event class with id $eventId not configured");
        }
        return $className;
    }

    public function getEventIdByClassName(DomainEventInterface $event): string
    {
        $eventClassName = get_class($event);
        $key = array_search($eventClassName, $this->eventClassByEventId, true);
        if ($key === false) {
            /** @noinspection ThrowRawExceptionInspection */
            throw new Exception("Event class with classname $eventClassName not configured");
        }
        return $this->eventClassByEventId[$key];
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }
}