<?php
declare(strict_types=1);

namespace Maxipost\AMQP;


use Maxipost\DomainEventSourcing\DomainEventInterface;
use Maxipost\FormStrategy\StrategyFactoryInterface;

class EventHydrator
{

    /**
     * @var StrategyFactoryInterface
     */
    private $strategyFactory;
    /**
     * @var Config
     */
    private $config;

    public function __construct(StrategyFactoryInterface $strategyFactory, Config $config)
    {

        $this->strategyFactory = $strategyFactory;
        $this->config = $config;
    }

    public function hydrate(Event $event): DomainEventInterface
    {
        return $this->strategyFactory
            ->__invoke($this->config->getClassNameByEventId($event->getEventId()))
            ->hydrate($event->getPayload());
    }

    public function extract(DomainEventInterface $event): array
    {
        return $this->strategyFactory
            ->__invoke(\get_class($event))
            ->extract($event);
    }
}