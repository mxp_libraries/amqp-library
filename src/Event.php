<?php
declare(strict_types=1);

namespace Maxipost\AMQP;

class Event
{

    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $eventId;
    /**
     * @var array
     */
    private $payload;

    public function __construct(string $id, string $eventId, array $payload)
    {
        $this->id = $id;
        $this->eventId = $eventId;
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEventId(): string
    {
        return $this->eventId;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }
}